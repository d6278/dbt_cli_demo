{% macro clean_tables_data() %}

    {% set query -%}
        DELETE FROM DEMO_DB.PUBLIC.CUSTOMERS;
        DELETE FROM DEMO_DB.PUBLIC.ORDERS;
    {%- endset %}

    {% do run_query(query) %}
{% endmacro %}