{{
  config(
    materialized = "incremental",
    schema = 'DBT_CLI_DEMO',
    database = 'DBT_CLI'
  )
}}

with row_rank_1 as (
    SELECT CAST(MD5_BINARY(NULLIF(CONCAT_WS('||',
        IFNULL(NULLIF(UPPER(TRIM(CAST(C.CUSTOMER_ID AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(O.ORDER_ID AS VARCHAR))), ''), '^^')
    ), '^^||^^')) AS BINARY(16)) AS LINK_CUSTOMER_ORDER_PK,
    CAST((MD5_BINARY(NULLIF(UPPER(TRIM(CAST(C.CUSTOMER_ID AS VARCHAR))), ''))) AS BINARY(16)) AS H_HASH_CUSTOMER,
    CAST((MD5_BINARY(NULLIF(UPPER(TRIM(CAST(O.ORDER_ID AS VARCHAR))), ''))) AS BINARY(16)) AS H_HASH_ORDER ,
    TO_DATE(current_date()) AS LOAD_DATE, 'PUBLIC' AS RECORD_SOURCE
    FROM {{source('demo_sample', 'CUSTOMERS')}} as C
    LEFT JOIN {{source('demo_sample', 'ORDERS')}} as O
    ON C.CUSTOMER_ID = O.CUSTOMER_ID
),

row_rank_2 as (
    SELECT LINK_CUSTOMER_ORDER_PK, H_HASH_CUSTOMER, H_HASH_ORDER , LOAD_DATE, RECORD_SOURCE,
     ROW_NUMBER() OVER(
               PARTITION BY LINK_CUSTOMER_ORDER_PK
               ORDER BY LOAD_DATE
           ) AS row_number
    FROM row_rank_1
    WHERE LINK_CUSTOMER_ORDER_PK IS NOT NULL
    AND H_HASH_CUSTOMER IS NOT NULL
    AND H_HASH_ORDER IS NOT NULL
    QUALIFY row_number = 1
),

source_emp_dep_data AS (
    SELECT a.LINK_CUSTOMER_ORDER_PK, a.H_HASH_CUSTOMER, a.H_HASH_ORDER, a.LOAD_DATE, a.RECORD_SOURCE
    FROM row_rank_2 AS a
)

SELECT * FROM source_emp_dep_data