{{
  config(
    materialized = "incremental",
    schema = 'DBT_CLI_DEMO',
    database = 'DBT_CLI'
  )
}}

with source_sat_emp_data as (
    SELECT CAST((MD5_BINARY(NULLIF(UPPER(TRIM(CAST(C.CUSTOMER_ID AS VARCHAR))), ''))) AS BINARY(16)) AS H_HASH_CUSTOMER ,
    CAST(MD5_BINARY(CONCAT_WS('||',
        IFNULL(NULLIF(UPPER(TRIM(CAST(MEMBER_NUMBER AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(FIRST_NAME AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(LAST_NAME AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(STREET_ADDRESS AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(ZIP AS VARCHAR))), ''), '^^')
    )) AS BINARY(16)) AS S_HASH_CUSTOMER,
    MEMBER_NUMBER , FIRST_NAME, LAST_NAME, STREET_ADDRESS, ZIP, TO_DATE(current_date()) AS LOAD_DATE, 'PUBLIC' as RECORD_SOURCE
    FROM  {{source('demo_sample', 'CUSTOMERS')}} as C

)

select * from source_sat_emp_data WHERE H_HASH_CUSTOMER IS NOT NULL