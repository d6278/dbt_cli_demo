{{
  config(
    materialized = "incremental",
    schema = 'DBT_CLI_DEMO',
    database = 'DBT_CLI'
  )
}}

with source_sat_emp_data as (
    SELECT CAST((MD5_BINARY(NULLIF(UPPER(TRIM(CAST(O.ORDER_ID AS VARCHAR))), ''))) AS BINARY(16)) AS H_HASH_ORDER ,
    CAST(MD5_BINARY(CONCAT_WS('||',
        IFNULL(NULLIF(UPPER(TRIM(CAST(PRODUCT_NAME AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(QUANTITY AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(DATE_OF_PURCHASE AS VARCHAR))), ''), '^^')
    )) AS BINARY(16)) AS S_HASH_CMR_PURCHASE,
    PRODUCT_NAME , QUANTITY, DATE_OF_PURCHASE, TO_DATE(current_date()) AS LOAD_DATE, 'PUBLIC' as RECORD_SOURCE
    FROM  {{source('demo_sample', 'ORDERS')}} as O

)

select * from source_sat_emp_data  WHERE H_HASH_ORDER IS NOT NULL