{{
  config(
    materialized = "incremental",
    schema = 'DBT_CLI_DEMO',
    database = 'DBT_CLI'
  )
}}

with source_sat_emp_data as (
    SELECT hc.H_HASH_CUSTOMER ,
    CAST(MD5_BINARY(CONCAT_WS('||',
        IFNULL(NULLIF(UPPER(TRIM(CAST(SSN_ENCRYPTED AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(CREDIT_CARD_ENCRYPTED AS VARCHAR))), ''), '^^')
    )) AS BINARY(16)) AS S_HASH_CUSTOMER_PII,
    SSN_ENCRYPTED , CREDIT_CARD_ENCRYPTED, TO_DATE(current_date()) AS LOAD_DATE, RECORD_SOURCE
    FROM {{ref('H_CUSTOMER')}} hc
    INNER JOIN {{source('demo_sample', 'CUSTOMERS')}} as C
    ON hc.CUSTOMER_ID = C.CUSTOMER_ID
    WHERE hc.H_HASH_CUSTOMER IS NOT NULL
)

select * from source_sat_emp_data