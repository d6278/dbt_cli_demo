{{
  config(
    materialized = "incremental",
    schema = 'DBT_CLI_DEMO',
    database = 'DBT_CLI'
  )
}}

with source_sat_emp_data as (
    SELECT ho.H_HASH_ORDER ,
    CAST(MD5_BINARY(CONCAT_WS('||',
        IFNULL(NULLIF(UPPER(TRIM(CAST(ITEM_DESCRIPTION AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(QUANTITY AS VARCHAR))), ''), '^^'),
        IFNULL(NULLIF(UPPER(TRIM(CAST(DATE_OF_PURCHASE AS VARCHAR))), ''), '^^')
    )) AS BINARY(16)) AS S_HASH_SALES,
    ITEM_DESCRIPTION , QUANTITY, DATE_OF_PURCHASE, TO_DATE(current_date()) AS LOAD_DATE, RECORD_SOURCE
    FROM {{ref('H_ORDERS')}} ho
    INNER JOIN {{source('demo_sample', 'ORDERS')}} as O
    ON ho.ORDER_ID = O.ORDER_ID
    WHERE ho.H_HASH_ORDER IS NOT NULL
)

select * from source_sat_emp_data