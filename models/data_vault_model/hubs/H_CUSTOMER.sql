{{
  config(
    materialized = "incremental",
    schema = 'DBT_CLI_DEMO',
    database = 'DBT_CLI'
  )
}}

with row_rank_1 as (
    SELECT CAST((MD5_BINARY(NULLIF(UPPER(TRIM(CAST(C.CUSTOMER_ID AS VARCHAR))), ''))) AS BINARY(16)) AS H_HASH_CUSTOMER ,
    C.CUSTOMER_ID , TO_DATE(current_date()) AS LOAD_DATE, 'PUBLIC' AS RECORD_SOURCE,
    ROW_NUMBER() OVER(
               PARTITION BY CUSTOMER_ID
               ORDER BY LOAD_DATE
           ) AS row_number
    FROM {{source('demo_sample', 'CUSTOMERS')}} as C
    where CUSTOMER_ID is not null
    QUALIFY row_number = 1
),

source_emp_data AS (
    SELECT a.H_HASH_CUSTOMER, a.CUSTOMER_ID, a.LOAD_DATE, a.RECORD_SOURCE
    FROM row_rank_1 AS a
)

SELECT * FROM source_emp_data


